<?php
/**
 * Created by PhpStorm.
 * User: majd2
 * Date: 2018-05-26
 * Time: 3:07 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'price',
        'quantity'
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];
}