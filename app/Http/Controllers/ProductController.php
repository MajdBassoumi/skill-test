<?php
/**
 * Created by PhpStorm.
 * User: majd2
 * Date: 2018-05-26
 * Time: 3:05 AM
 */

namespace App\Http\Controllers;


use App\Http\Requests\StoreProductRequest;
use App\Product;

class ProductController extends Controller
{

    public function __construct()
    {

    }

    public function index()
    {
        $products = Product::get();
        $total =0;
        if (count($products) > 0){
            foreach ($products as $product){
                $total += ($product->price * $product->quantity);
            }
        }
        return view('welcome', compact('products', 'total'));
    }


    public function store(StoreProductRequest $request)
    {
        $attributes = $request->all();
        Product::create([
            'name' => $attributes['name'],
            'price' => $attributes['price'],
            'quantity' => $attributes['quantity'],
        ]);
        return response()->json(['redirect' => '/products'], 200);

    }

    public function edit(Product $product)
    {
        if (is_null($product)){
            return response()->json(['redirect' => '/products'], 200);
        }else{
            return view('edit', compact('product'));
        }
    }

    public function update(StoreProductRequest $request, Product $product)
    {
        $attributes = $request->all();

        $product->update([
            'name' => $attributes['name'],
            'price' => $attributes['price'],
            'quantity' => $attributes['quantity'],
        ]);
        return response()->json(['redirect' => '/products'], 200);

    }


}