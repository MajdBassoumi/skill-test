<!DOCTYPE html>
<html lang="en">
<head>
    <title>Skill Test</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>


@if(count($errors))
    <hr>
    <div class="form-group">
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

<div class="container">
    <h2>Products</h2>
    <form id="edit_product">
        {{ csrf_field() }}
        {{method_field('PATCH')}}

        <div class="form-group">
            <label for="name">Product name:</label>
            <input type="text" class="form-control" id="name" placeholder="Product name" name="name" value="{{$product->name}}">
        </div>
        <div class="form-group">
            <label for="quantity">Quantity in stock:</label>
            <input type="number" class="form-control" id="quantity" placeholder="Quantity in stock:" name="quantity" value="{{$product->quantity}}">
        </div>
        <div class="form-group">
            <label for="price">Price per item</label>
            <input type="number" class="form-control" id="price" placeholder="Price per item" name="price" value="{{$product->price}}">
        </div>
        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>


</body>
</html>


<script>

    $("#edit_product").submit(function (e) {
        e.preventDefault();


        var url = "/products/{{$product->id}}";

        $.ajax({
            type: "POST",
            url: url,
            data: $("#edit_product").serialize(),
            success: function (response) {
                console.log('Submission was successful.');
                window.location.href = response.redirect;
            }
        });

    });

</script>
