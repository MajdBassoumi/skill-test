<!DOCTYPE html>
<html lang="en">
<head>
    <title>Skill Test</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>


@if(count($errors))
    <hr>
    <div class="form-group">
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

<div class="container">
    <h2>Products</h2>
    <form id="store_product">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Product name:</label>
            <input type="text" class="form-control" id="name" placeholder="Product name" name="name">
        </div>
        <div class="form-group">
            <label for="quantity">Quantity in stock:</label>
            <input type="number" class="form-control" id="quantity" placeholder="Quantity in stock:" name="quantity">
        </div>
        <div class="form-group">
            <label for="price">Price per item</label>
            <input type="number" class="form-control" id="price" placeholder="Price per item" name="price">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

<div class="container">

    @if(isset($products))
        @foreach($products as $product)
            <br>
            <hr>
            <br>
            <div class="form-group  inline">
                <p>Product name: {{$product->name}}</p>
                <p>Quantity in stock: {{$product->quantity}}</p>
                <p>Price per item: {{$product->price}}</p>
                <p>Datetime submitted: {{$product->created_at}}</p>
                <p>Total value number: {{$product->quantity * $product->price}}</p>
                <a href="products/{{$product->id}}/edit"> Edit</a>
            </div>

        @endforeach
        @if(isset($total) and count($products)>0)
            <br>
            <hr>
            <br>
            <div class="form-group">
                <p>Total total value: {{$total}}</p>
            </div>
        @endif
    @endif

</div>

</body>
</html>


<script>

    $("#store_product").submit(function (e) {
        e.preventDefault();


        var url = "/products";

        $.ajax({
            type: "POST",
            url: url,
            data: $("#store_product").serialize(),
            success: function (response) {
                console.log('Submission was successful.');
                window.location.href = response.redirect;
            }
        });

    });

</script>

